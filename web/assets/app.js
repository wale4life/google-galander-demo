



var app = new Vue({
    el: '#app',
    data: {
        todo: {
            title: '',
            date: '',
            file: '',
        },
        todos: [
            { title: 'Lets ride', date: '11/11/11', id: 1},
            { title: 'Again Lets ride', date: '11/11/11', id: 2, },
        ],
        calander: null,
    },
    mounted() {
        fetch('/todos').then(res =>res.json()).then(data => {
            this.todos = data.todos;
            console.log('Todos: ', this.todos)
            console.log('Data: ', data)
        })
        
    },
    methods: {
        handleSubmit(evt) {

            const formData = new FormData(evt.target);
            fetch('todos/create', {
                method: 'POST',
                body: formData,
            }).then(res => res.json()).then(res => {
                console.log(res)
                this.addTodo(res.todo)
                evt.target.reset();
            }).catch(e => {
                console.log(e)
            })
        },
        getFile(evt) {
            this.todo.file = evt.target.files[0]
            console.log(this.todo.file)
        },
        addTodo(todo) {
            this.todos.push(todo)
        },
        readFile(file) {
            reader = new FileReader()
            console.log(file)
            return reader.readAsDataURL(file);
        },
        isImage(file) {
            return /\.(jpe?g|png|gif|bmp|webm)$/i.test(file)
        },
        getLink(file) {
            return window.location.origin + file
        },
    },
})
