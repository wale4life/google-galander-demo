const views = require('koa-views');
const path = require('path');

// setup views mapping .html
module.exports = views(path.join(__dirname, '/../web'), {
//   map: { html: 'swig' }
});