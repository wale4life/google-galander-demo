const { google } = require("googleapis");
require('dotenv').config()



const token = process.env['REFRESH_TOKEN'];
const client_id = process.env.CLIENT_ID;
const client_secret = process.env.CLIENT_SECRET;
const redirect_url = "localhost:3000";



const loadClient = () => {
  const auth = new google.auth.OAuth2(client_id, client_secret, redirect_url);

  auth.setCredentials({ refresh_token: token });
  google.options({ auth });
};


// const oauth2Client = new google.auth.OAuth2(
//     client_id,
//     client_secret,
//     redirect_url
//   );
//   oauth2Client.setCredentials({ refresh_token: YOUR_REFRESH_TOKEN });
  
// const calendar = google.calendar({ version: "v3", auth: oauth2Client });




// Create new Event



const createEvent = (event) => {
    // Instance of calendar
    const calendar = google.calendar({ version: "v3" });
  
    // Start date set to next day 3PM
    const startTime = new Date(event.start);

    // startTime.setDate(startTime.getDate() + 1);
    // startTime.setHours(15, 0, 0, 0);
  
    // // End time set 1 hour after start time
    // const endTime = new Date(startTime.getTime());
    // endTime.setMinutes(startTime.getMinutes() + 60);
  
    // const newEvent = {
    //   calendarId: "primary",
    //   resource: {
    //     start: {
    //       dateTime: startTime.toISOString(),
    //     },
    //     end: {
    //       dateTime: endTime.toISOString(),
    //     },
    //     summary: '',
    //   },
    // };
  
    return new Promise( (resolve, reject) => {
      event.start = {
        dateTime: startTime.toISOString(),
      }
      event.end = {
        dateTime: startTime.toISOString(),
      }
      const payload = {
        calendarId: 'primary',
        resource: event,
      }
      console.log(event)
      calendar.events.insert(payload, (err, event) => {
        // console.log(err)
          if (err) reject(err);
          resolve(event.data);
      });
    })
    
  };



  // Get Events within the time slot
  const getEvents = (instance, event) => {
    
    const calendarId = "primary";
    const query = {
      resource: {
        timeMin: event.start.dateTime,
        timeMax: event.end.dateTime,
        items: [{ id: calendarId }],
      },
    };
  
    // Return a list of events
    return new Promise( (resolve, reject) => {
      instance.freebusy
          .query(query)
          .then((response) => {
            const { data } = response;
            const { calendars } = data;
            const { busy } = calendars[calendarId];
      
            resolve(data.items)
          })
          .catch((err) => {
            reject(err);
          });
      })
  };




// List events
const listEvents = () => {
    const calendar = google.calendar({ version: "v3" });
    // console.log('CAL: ', calendar)
    return new Promise((resolve, reject) => {
        calendar.events.list({
            calendarId: "primary",
            timeMin: new Date().toISOString(), // Look for events from now onwards
            maxResults: 20, // Limit to 10 events
            singleEvents: true,
            orderBy: "startTime",
        }, (err, res) => {
          // console.log(res.data.items)
            if (err) reject(console.log("The API returned an error: " + err))
            resolve(res.data.items)
        }
    )
  });
}


module.exports = {
    listEvents,
    loadClient,
    createEvent,
    getEvents,
}
