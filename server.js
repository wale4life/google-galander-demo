const http = require('http')
const fs = require('fs')

console.log('Starting server on http://localhost:8100')
http.createServer((req, res) => {
    fs.readFile('web/index.html', (err, data) => {

        res.writeHead(200, { 'Content-Type': 'text/html'})
        // res.write("Welcome to Node JS")
        res.write(data)
        res.end();
    })
}).listen(8100)