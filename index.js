
const logger = require('koa-logger');
const router = require('@koa/router')();
const koaBody = require('koa-body');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');
const serve = require('koa-static');
const uuid = require('uuid')
const mime = require('mime-types')
// const render = require('./lib/render');

const Koa = require('koa');
const path = require('path')
const os = require('os');
const fs = require('fs')


const queries = require('./db/queries/todos')
const { loadClient, getEvents, listEvents, createEvent } = require("./libs/google/calander-client");



// app instance
const app = module.exports = new Koa();


loadClient();

// CORS
app.use(cors({
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET,POST,PUT,DELETE'
}))
// middlewares
app.use(logger());
// app.use(render);
app.use(serve(path.join(__dirname, '/web')))
app.use(koaBody({multipart: true, uploadDir: './web/uploads'}));



    
    /**
     * Tudo listing.
     */
    async function welcome(ctx) {
        
        await ctx.render('/');
    }

    /**
     * Tudo listing.
     */
    const list = async ( ctx ) => {
        
        const todos = await queries.getTodos()
        const events = await listEvents();
        ctx.body = { todos, events };
    }

    /**
     * Show todo :id.
     */
    async function show(ctx) {
        const id = ctx.params.id;
        const post = posts[id];
        if (!post) ctx.throw(404, 'invalid Todo id');
        await ctx.render('show', { post: post });
    }

    /**
     * Create a todo.
     */
    async function create( ctx) {
        let todo = ctx.request.body;
        const { path, type } = ctx.request.files.file
        const fileExtension = mime.extension(type)
        const uploadPath = `/uploads/${uuid.v1()}.${fileExtension}`;
        await fs.copyFile(path, './web' + uploadPath, err => {
            if(err) new Error('ERR: File upload failed!')
        });
        todo.file = uploadPath
        console.log('RAW TODO: ', todo)
        const googleEvent = {
            start:  todo.date,
            summary: todo.title,
        }
        const id = await queries.addTodo(todo);
        console.log('ID', id)
        todo = await queries.singleTodo(id);
        console.log('SUBMITTED TODO: ', todo)
        
        const gevt = await createEvent(googleEvent)
        console.log('GEVT: ', gevt)
        console.log('Uploaded TODO', todo)
        ctx.body = {
            status: "Successful",
            message: "added successfully",
            todo: todo[0],
        }
    }



    
// route definitions
router.get('/', welcome)
.get('/todos', list )
.get('/todos/:id', show)
.post('/todos/create', create)
app.use(router.routes());


// app.use(async ctx => {
//     fs.readFile('web/index.html', (err, data) => {
//         ctx.body = data
//         // res.write(data)
//     })
//     // ctx.body = 'Hello World';
// });

console.log('Starting server on http://localhost:8100')
app.listen(8100);