# A Calnder app (Test Project)

### How to install
Both the client and server are contained within this project. While the client is set to run on port 8080, the server is configured to run on port 8100. clone the repo to begin

-----------------------------------------------

Server Setup

-----------------------------------------------


- run `npm install`
- `npm run start`
- Setup `mysql` server to enable db connection
- run `npm run migrate-db && npm run seed:db` this will setup the database





------------------------------------------------

Frontend Setup 

------------------------------------------------

- run `npm install`
- `npm run start-frontend`