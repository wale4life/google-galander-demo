
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('todos').truncate().then(function () {
      // Inserts seed entries
      return knex('todos').insert([
        {
          // id: 1, 
          title: 'Get a new wrist watch',
          date: '11/21/2020',
          file: '',
        },
        {
          // id: 2, 
          title: 'Take acourse on quantim physics', 
          date: '11/29/2020',
          file: '',
        },
        {
          // id: 3, 
          title: 'Get a new telescope for the winter solstice moon',
          date: '12/21/2020',
          file: '',
        },
      ]);
    });
};
