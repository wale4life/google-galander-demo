const knex = require('../connection');





module.exports = {
    getTodos: () => knex('todos').select('*'),
    singleTodo: (id) => knex('todos')
                        .select('*')
                        .where({id: parseInt(id)}),
    addTodo: (todo) => knex('todos').insert({ ...todo, createdAt: new Date()}).returning('*'),
}
