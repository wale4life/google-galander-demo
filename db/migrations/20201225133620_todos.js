
exports.up = function(knex) {
  return knex.schema.createTable('todos', (table) => {
      table.increments();
      table.string('title');
      table.string('date');
      table.string('file')
      table.string('createdAt');
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('todos');
};
