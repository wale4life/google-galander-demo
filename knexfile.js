const path = require('path');

const BASE_PATH = path.join(__dirname, 'db');

module.exports = {
//   test: {
//     client: 'pg',
//     connection: 'mysql://username:password@localhost:3306/todo_demo',
//     migrations: {
//       directory: path.join(BASE_PATH, 'migrations')
//     },
//     seeds: {
//       directory: path.join(BASE_PATH, 'seeds')
//     }
//   },
  development: {
    client: 'mysql',
    connection: {
        host: '127.0.0.1',
        user: 'root',
        password: '',
        database: 'todo_demo',
    },
    migrations: {
      directory: path.join(BASE_PATH, 'migrations')
    },
    seeds: {
      directory: path.join(BASE_PATH, 'seeds')
    }
  },
  sqlite: {
    client: 'sqlite3',
    connection: {
        filename: path.join(BASE_PATH, 'db') + '/todo-db.sqlite'
    },
    migrations: {
      directory: path.join(BASE_PATH, 'migrations')
    },
    seeds: {
      directory: path.join(BASE_PATH, 'seeds')
    }
  }
};